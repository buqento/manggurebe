<?php
	include "config/conn.php";
	include "config/fungsi_indotgl.php";
	$url=$_GET[id];
	$pecah = explode("/",$url);
	$query = mysql_query("SELECT * FROM t_article WHERE ID=$pecah[0]") or die("Query Error");
	while($kolom = mysql_fetch_array($query))
	{
		$id = $kolom['ID'];
		$tanggal = $kolom['post_date'];
		$deksripsi = $kolom['post_description'];
		$konten = $kolom['post_content'];
		$judul = $kolom['post_title'];
		$seo = $kolom['post_seo'];
		$kategori = $kolom['post_category'];
		$hit = $kolom['hit'];		
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $deksripsi;?>">
    <meta name="author" content="Buqento Richard Onaola">
    <meta name="keywords" content="Artikel, Pemrograman, Komputer, Delphi, Desain Web, SQL, Yii, Framework">
    <link rel="shortcut icon" href="assets/logo.html">
    <title><?php echo $judul;?></title>

    <!-- Bootstrap -->
    <link href="../../assets/css/bootstrap.css" rel="stylesheet">
    <link href="../../assets/css/progress.css" rel="stylesheet">
	<link href="../../assets/css/style.css" rel="stylesheet">
	<link href="../../assets/css/font-opensans.css" rel="stylesheet" type="text/css" media="screen" > 
	<link rel="stylesheet" href="../../assets/font-awesome-4.1.0/css/font-awesome.min.css">
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

		
  </head>
  <body>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&appId=387708794632040&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>


	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container">
	  <a class="navbar-brand" href="index.php">www.MANGGUREBE.com</a>
		<!-- Brand and toggle get grouped for better mobile display
		<img src="http://placehold.it/350x150"> -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		</div>
	

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">

			<li><a href="../../index.php"><i class="fa fa-home fa-lg"></i> BERANDA</a></li>

		  </ul>
	  </div>
	</nav>



<div class="container border-all" id="containerx">
	<div class="row">

  		<!--SHOW ARTIKEL-->
		<div class="col-xs-12 col-sm-12 col-md-6" id="content_news">
      	<div class="color-cyan konten">

		<ol class="breadcrumb">
		  <li>&nbsp;&nbsp;<a href="../../index.php">Beranda</a></li>
		  <li class="active"><?php echo $kategori;?></li>
		</ol>

		<h1 class="judul-berita-detail"><?php echo $judul;?></h1>
			<div class="tanggal upper">
				<?php 
					if($kategori == 'delphi'){$ikon = 'university';}
					if($kategori == 'html'){$ikon = 'globe';}
					if($kategori == 'php'){$ikon = 'globe';}
					if($kategori == 'sql'){$ikon = 'database';}
					if($kategori == 'yii'){$ikon = 'leaf';}
				?>
				<i class="fa fa-<?php echo $ikon;?>"></i> <?php echo $kategori;?>&nbsp; 
				&nbsp;<i class="fa fa-calendar"></i> <?php echo tgl_indo($tanggal);?>
				&nbsp;<i class="fa fa-eye"></i> <?php echo $hit;?>
			</div>
		<hr class="featurette-divider">
		
		<div class="fb-share-button" data-href="http://manggurebe.com/<?php echo $kategori;?>/<?php echo $id;?>/<?php echo $seo;?>.html"></div>
		
		<span class="isi-konten"><?php echo $konten;?></span>
		<?php $tambah = $hit++;	$kurang = $hit - 1;?>
		

		<?php
		$old = $id-1;
		$o_que = mysql_query("SELECT post_category, post_seo FROM t_article WHERE ID=$old");
		$o_col = mysql_fetch_array($o_que);
		$o_ctg = $o_col['post_category'];
		$o_seo = $o_col['post_seo'];
		if($o_seo == ""){$o_status = "previous disabled";$o_link = "#";
		}else{$o_status = "previous";$o_link="../../$o_ctg/$old/$o_seo.html";}
		
		$new = $id+1;
		$n_que = mysql_query("SELECT post_category, post_seo FROM t_article WHERE ID=$new");
		$n_col = mysql_fetch_array($n_que);
		$n_ctg = $n_col['post_category'];
		$n_seo = $n_col['post_seo'];
		if($n_seo == ""){$n_status = "next disabled";$n_link = "#";
		}else{$n_status = "next";$n_link="../../$n_ctg/$new/$n_seo.html";}
		?>

		<ul class="pager">
		  <li class="<?php echo "$o_status";?>"><a href="<?php echo $o_link;?>">&larr; Older</a></li>
		  <li class="<?php echo "$n_status";?>"><a href="<?php echo $n_link;?>">Newer &rarr;</a></li>
		</ul>
		

		<!--Can be one of "box_count", "button_count", "button", "link", "icon_link", or "icon".-->
		
		<div class="fb-comments" data-href="http://manggurebe.com/<?php echo $kategori;?>/<?php echo $id;?>/<?php echo $seo;?>.html" data-layout="button_count" data-width="100%" data-numposts="3" data-colorscheme="light"></div>


		<?php } mysql_query("UPDATE t_article SET hit=".$hit++." WHERE ID=$pecah[0]");?>


		</div>
		<hr class="featurette-divider">
		</div>
  		<!--END SHOW ARTIKEL-->



  		<!--OTHER ARTIKEL-->
  		<div class="col-xs-12 col-sm-6 col-md-3" align="center">
		<div class="row">
		
		<?php
		
		include "config/conn.php";
		$query = mysql_query("SELECT * FROM t_article WHERE ID!=$id  AND post_category='$kategori' ORDER BY ID DESC LIMIT 5");
		while($kolom = mysql_fetch_array($query))
		{
		?>
		
		  <div class="col-sm-6 col-md-12">
			<div class="thumbnail">
			  <!--img src="http://placehold.it/350x150"-->
			  <div class="caption">
				<a href="../../<?php echo $kategori;?>/<?php echo $kolom['ID'];?>/<?php echo $kolom['post_seo'];?>.html">
					<h3 class="judul sans">
					<?php echo $kolom['post_title'];?>
					</h3>
				</a>
				<span class="intro-berita">
				<?php					
				$keterangan=$kolom['post_description'];
				$kata=strtok($keterangan," ");
				for ($i=1;$i<=20;$i++)
				{
				echo($kata);
				echo(" ");
				$kata=strtok(" ");
				}
				echo("");
				echo("...");
				?>
				</span><br>
				<br>


				<a href="../../<?php echo $kategori;?>/<?php echo $kolom['ID'];?>/<?php echo $kolom['post_seo'];?>.html" class="btn btn-primary" role="button">Selengkapnya <i class="fa fa-angle-double-right"></i> </a>
			  </div>
			</div>
		  </div>

		<?php } ?>

		</div>
		</div>
  		<!--END OTHER ARTIKEL-->




 		<!--BACA JUGA-->
  		<div class="col-xs-12 col-sm-12 col-md-3">

			<div class="row">
		
			<?php		
			$query = mysql_query("SELECT * FROM t_article ORDER BY last_update DESC LIMIT 7");
			while($more = mysql_fetch_array($query)){
				$id = $more['ID'];
				$tanggal = $more['post_date'];
				$deksripsi = $more['post_description'];
				$konten = $more['post_content'];
				$judul = $more['post_title'];
				$seo = $more['post_seo'];
				$kategori = $more['post_category'];
				$hit = $more['hit'];		

				if($kategori == 'delphi'){
					$warna_latar = 'primary';}
				if($kategori == 'html'){
					$warna_latar = 'danger';}
				if($kategori == 'php'){
					$warna_latar = 'danger';}
				if($kategori == 'sql'){
					$warna_latar = 'warning';}
				if($kategori == 'yii'){
					$warna_latar = 'success';}
	
			?>


			  <div class="col-sm-12 col-md-12">

			    <div class="media">
				<a class="pull-left" href="../../<?php echo $kategori;?>/<?php echo $id;?>/<?php echo $seo;?>.html">
					<div class="kotak <?php echo $warna_latar;?>" align="center"><?php echo substr($judul,0,1);?></div>
				</a>
				<div class="media-body">
					<a href="../../<?php echo $kategori;?>/<?php echo $id;?>/<?php echo $seo;?>.html">
						<h3 class="judul sans">
						<?php echo $judul;?>
						</h3>
						
					</a>
				</div>

				</div>
		<hr class="featurette-divider">

			  </div>

		<?php } ?>
		</div>
		
		</div>
  		<!--END BACA JUGA-->


</div>	
</div>



<div class="clearfix"></div>    

	<div class="container footer">
	
		<div class="col-xs-12 col-sm-6 col-md-6">
		
				<address>
				  	<a href="../../index.php"><h4 class="judul-footer">MANGGUREBE | All About Programming</h4></a>
				  	All Post Written by: Buqento Richard Onaola<br>
					Trainner - Freelancer - Programmer<br>
					P: 7D7A65F6
					
				</address>
		</div>
	
		<div class="col-xs-12 col-sm-6 col-md-6">
		
			<p class="pull-right">
				<a class="btn btn-default btn-sm" href="#"><i class="fa fa-arrow-circle-up fa-3x"></i></a>
			</p>

			<div>
				<span><a href="../../index.php">BERANDA</a>&nbsp;&middot;&nbsp;</span>
				<span><a href="#">Delphi</a>&nbsp;&middot;&nbsp;</span>
				<span><a href="#">Web</a>&nbsp;&middot;&nbsp;</span>
				<span><a href="#">SQL</a>&nbsp;&middot;&nbsp;</span>
				<span><a href="#">Yii</a></span>
				<br>
				<span>&copy; 2014 buqento. All rights reserved. </span><br>
				<small>Powered by <a rel="author" href="https://profiles.google.com/105878573540112060931" target="_blank">Buqento Richard</a></small><br>
				<a href="https://plus.google.com/105878573540112060931" rel="publisher" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a>
				<a href="http://twitter.com/buqento" target="_blank"><i class="fa fa-twitter-square fa-lg"></i></a>
				<a href="http://facebook.com/xnovo" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a>
				
			</div>
	
		</div>
	
	</div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../assets/js/bootstrap.min.js"></script>
  </body>
</html>
