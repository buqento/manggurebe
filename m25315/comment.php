<?php
	include "config/conn.php";
	include "config/fungsi_indotgl.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Kumpulan Artikel Komputer">
    <meta name="author" content="Buqento Richard Onaola">
    <meta name="keywords" content="Artikel, Pemrograman, Komputer, Hardware, Software, Jaringan">
    <link rel="shortcut icon" href="assets/logo.html">
    <title>Manggurebe | All About Programming</title>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/progress.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/font-opensans.css" rel="stylesheet" type="text/css" media="screen" > 
	<link rel="stylesheet" href="assets/font-awesome-4.1.0/css/font-awesome.min.css">
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script language="javascript" src="assets/js/jquery-1.2.3.pack.js"></script>
	
  </head>
  <body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&appId=387708794632040&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<nav class="navbar navbar-inverse" role="navigation">
	  <div class="container">
	  <a class="navbar-brand" href="index.php">All About Programming</a>
		<!-- Brand and toggle get grouped for better mobile display
		<img src="http://placehold.it/350x150"> -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		</div>
	

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">

			<li><a href="index.php"><i class="fa fa-home fa-lg"></i> BERANDA</a></li>
			<li><a href="#"><i class="fa fa-university"></i> Delphi</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-globe"></i> Desain Web
  			  </a>
                  <ul class="dropdown-menu">
                    <li><a href="#">HTML</a></li>
                    <li><a href="#">PHP</a></li>
                  </ul>
            </li>
			<li><a href="#"><i class="fa fa-database"></i> SQL</a></li>
			<li><a href="#"><i class="fa fa-leaf"></i> Yii Framework</a></li>

		  </ul>
	  </div>
	</nav>



<div class="container border-all">
	<div class="row">


 		<!--BACA JUGA-->
  		<div class="col-xs-12 col-sm-12 col-md-12">

			<div class="row">
			<h1>Comment</h1>
			
			<div class="fb-comments" data-href="http://manggurebe.com/comment" data-numposts="5" data-colorscheme="light"></div>

			</div>
		
		
		</div>
  		<!--END BACA JUGA-->


</div>	
</div>



<div class="clearfix"></div>    

<?php include("footer.php");?>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>
