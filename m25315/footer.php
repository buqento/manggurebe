<!DOCTYPE html>
<html lang="en">
  <head></head>
  
  <body>
  
	<div class="container footer">
	
		<div class="col-xs-12 col-sm-6 col-md-6">
		
				<address>
				  	<a href="<?php $_SERVER['DOCUMENT_ROOT'];?>"><h4 class="judul-footer">MANGGUREBE | All About Programming</h4></a>
				  	All Post Written by: Buqento Richard Onaola<br>
					Trainner - Freelancer - Programmer<br>
					P: 7D7A65F6
					
				</address>
		</div>
	
		<div class="col-xs-12 col-sm-6 col-md-6">
		
			<p class="pull-right">
				<a class="btn btn-default btn-sm" href="#"><i class="fa fa-arrow-circle-up fa-3x"></i></a>
			</p>

			<div>
				<span><a href="<?php $_SERVER['DOCUMENT_ROOT'];?>">BERANDA</a>&nbsp;&middot;&nbsp;</span>
				<span id="linkdetail"><a href="kategori/delphi">Delphi</a>&nbsp;&middot;&nbsp;</span>
				<span id="linkdetail"><a href="kategori/html">HTML</a>&nbsp;&middot;&nbsp;</span>
				<span id="linkdetail"><a href="kategori/php">PHP</a>&nbsp;&middot;&nbsp;</span>
				<span id="linkdetail"><a href="kategori/sql">SQL</a>&nbsp;&middot;&nbsp;</span>
				<span id="linkdetail"><a href="kategori/yii">Yii</a></span>
				<br>
				<span>&copy; 2014 buqento. All rights reserved. </span><br>
				<small>Powered by <a rel="author" href="https://profiles.google.com/105878573540112060931" target="_blank">Buqento Richard</a></small><br>
				<a href="https://plus.google.com/105878573540112060931" rel="publisher" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a>
				<a href="http://twitter.com/buqento" target="_blank"><i class="fa fa-twitter-square fa-lg"></i></a>
				<a href="http://facebook.com/xnovo" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a>
			</div>
	
		</div>
	
	</div>

  </body>
</html>
