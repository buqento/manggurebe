<?php
	include "config/conn.php";
	include "config/fungsi_indotgl.php";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Kumpulan Artikel Komputer">
    <meta name="author" content="Buqento Richard Onaola">
    <meta name="keywords" content="Artikel, Pemrograman, Komputer, Delphi, Desain Web, SQL, Yii, Framework">
    <link rel="shortcut icon" href="assets/logo.html">
    <title>Manggurebe | All About Programming</title>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/progress.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/font-opensans.css" rel="stylesheet" type="text/css" media="screen" > 
	<link rel="stylesheet" href="assets/font-awesome-4.1.0/css/font-awesome.min.css">
     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script language="javascript" src="assets/js/jquery-1.2.3.pack.js"></script>
	<script language="javascript">
	$(document).ready(function(){	
		$('#linkdetail a').click(function(){
			var url = $(this).attr('href');	
			//$('#container').html('Loading...');
			$('#content_news').load(url)
			return false;
		});
	});
	</script>
	<script src="https://apis.google.com/js/platform.js" async defer>
	  {lang: 'id'}
	</script>


	<script type="text/javascript">
		$(document).ready(function(){
			tampil_data(1);	
			pilih_pages();
//			$("#1").css({'background-color' : '#FF6600'}); //agar paging button bagian pertama berubah warna, karena page 1 yang pertama di jalankan
		});
		//untuk memunculkan animasi loading
		//sebulumnya properti class loading sudah di set display:none
		function show_loader(){
			$(".loading").fadeIn(200);
		}
		//menghilangkan animasi loading
		function hide_loader(){
			$(".loading").fadeOut(200);
		}
		//menampilkan data
		function tampil_data(pages){
			show_loader();
			$("#tampil_data").load("content_news.php?page=" + pages, hide_loader);
		}
		function pilih_pages(){
			$("#paging_button li").click(function(){
				var pages = this.id;
				tampil_data(pages);
			//	$("#paging_button li").css({'background-color' : ''});
			//	$(this).css({'background-color' : '#FF6600'}); //ganti warna paging button yang di click
				return false;
			});
		}
	</script>

  </head>
  <body>

	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container">
	  <a class="navbar-brand" href="index.php">www.MANGGUREBE.com</a>
		<!-- Brand and toggle get grouped for better mobile display
		<img src="http://placehold.it/350x150"> -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Navigasi</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		</div>
	

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">

			<li><a href="index.php"><i class="fa fa-home fa-lg"></i> BERANDA</a></li>

			<li id="linkdetail"><a href="kategori/delphi"><i class="fa fa-university"></i> Delphi</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-globe"></i> Desain Web
  			  </a>
                  <ul class="dropdown-menu">
                    <li id="linkdetail"><a href="kategori/html">HTML</a></li>
                    <li id="linkdetail"><a href="kategori/php">PHP</a></li>
                  </ul>
            </li>
			<li id="linkdetail"><a href="kategori/sql"><i class="fa fa-database"></i> SQL</a></li>
			<li id="linkdetail"><a href="kategori/yii"><i class="fa fa-leaf"></i> Yii Framework</a></li>
		
		  </ul>
	  </div>
	</nav>




<div class="container border-all" id="containerx">
	<div class="row">




  		<!--SHOW ARTIKEL-->
		<div class="col-xs-12 col-sm-12 col-md-7" id="content_news">
      	<div class="color-cyan konten">


		<div id="paging_button">
			<?php
			
			include "config/conn.php";
			$limit = 5;
			$query = mysql_query("SELECT * FROM t_article");
			$count = mysql_num_rows($query);
			$pages = ceil($count/$limit);
					echo "<ul class='pagination pagination-sm'>";
					for($i=1; $i<=$pages; $i++){
						echo '<li id="'.$i.'"><a href="#">'.$i.'</a></li>';
					}
					echo "<li class=\"loading\">&nbsp;<img src='assets/images/small_load.gif'></span></li>";
					echo "</ul>";
			?>
			
		</div>
		<div id="tampil_data"></div>
		<div id="paging_button">
			<?php
			
			include "config/conn.php";
			$limit = 5;
			$query = mysql_query("SELECT * FROM t_article");
			$count = mysql_num_rows($query);
			$pages = ceil($count/$limit);
					echo "<ul class='pagination pagination-sm'>";
					for($i=1; $i<=$pages; $i++){
						echo '<li id="'.$i.'"><a href="#">'.$i.'</a></li>';
					}
					echo "<li class=\"loading\">&nbsp;<img src='assets/images/small_load.gif'></span></li>";
					echo "</ul>";
			?>
		</div>
		
		<?php //include "paging.php";?>

		</div>
		</div>
  		<!--END SHOW ARTIKEL-->

 



 		<!--KATEGORI-->
  		<div class="col-xs-12 col-sm-12 col-md-2">

		<div class="kategori">

		<!--div class="g-person" data-width="180" data-href="//plus.google.com/u/0/105878573540112060931" data-rel="author"></div>

		<hr class="featurette-divider"-->
		
		  <div class="panel panel-primary">
			<div class="panel-heading sans"><i class="fa fa-university"></i> <B>Delphi</B></div>
			  <ul class="list-group">
			  <?php
				$t = mysql_query("SELECT ID, post_title, post_seo FROM t_article WHERE post_category = 'delphi' ORDER BY ID DESC LIMIT 0,3");
				while($rt = mysql_fetch_array($t)){
					echo "<li class='list-group-item'><a href='delphi/$rt[ID]/$rt[post_seo].html'>$rt[post_title]</a></li>";
				}
			  ?>
	
			   </ul>
		  </div><!-- end panel-primary --> 

		  <div class="panel panel-danger">
			<div class="panel-heading sans"><i class="fa fa-globe"></i> <B>Desain Web</B></div>
			  <ul class="list-group">
			  <?php
				$t = mysql_query("SELECT ID, post_title, post_seo FROM t_article WHERE post_category = 'php' ORDER BY ID DESC LIMIT 0,3");
				while($rt = mysql_fetch_array($t)){
					//if($rt['post_category']=='php'){
						echo "<li class='list-group-item'><a href='html/$rt[ID]/$rt[post_seo].html'>$rt[post_title]</a></li>";
					//}
					//else{
						//echo "<li class='list-group-item'><a href='php/$rt[ID]/$rt[post_seo].html'>$rt[post_title]</a></li>";
				//	}
				}
			  ?>
	
			   </ul>
		  </div><!-- end panel-danger --> 

		  <div class="panel panel-warning">
			<div class="panel-heading sans"><i class="fa fa-database"></i> <B>SQL</B></div>
			  <ul class="list-group">
			  <?php
				$t = mysql_query("SELECT ID, post_title, post_seo FROM t_article WHERE post_category = 'sql' ORDER BY ID DESC LIMIT 0,3");
				while($rt = mysql_fetch_array($t)){
					echo "<li class='list-group-item'><a href='sql/$rt[ID]/$rt[post_seo].html'>$rt[post_title]</a></li>";
				}
			  ?>
	
			   </ul>
		  </div><!-- end panel-warning --> 

		  <div class="panel panel-success">
			<div class="panel-heading sans"><i class="fa fa-leaf"></i> <B>Yii Framework</B></div>
			  <ul class="list-group">
			  <?php
				$t = mysql_query("SELECT ID, post_title, post_seo FROM t_article WHERE post_category = 'yii' ORDER BY ID DESC LIMIT 0,3");
				while($rt = mysql_fetch_array($t)){
					echo "<li class='list-group-item'><a href='yii/$rt[ID]/$rt[post_seo].html'>$rt[post_title]</a></li>";
				}
			  ?>
	
			   </ul>
		  </div><!-- end panel-success --> 


		</div>
		</div>
  		<!--END KATEGORI-->



 		<!--BACA JUGA-->
  		<div class="col-xs-12 col-sm-12 col-md-3">

			<div class="row">
		
			<?php		
			$query = mysql_query("SELECT * FROM t_article ORDER BY last_update DESC LIMIT 5");
			while($kolom = mysql_fetch_array($query)){
			
			
				if($kolom['post_category'] == 'delphi')
					$ctg = 'delphi'; $warna_latar = 'primary';
				if($kolom['post_category'] == 'html')
					$ctg = 'html'; $warna_latar = 'danger';
				if($kolom['post_category'] == 'php')
					$ctg = 'php'; $warna_latar = 'danger';
				if($kolom['post_category'] == 'sql')
					$ctg = 'sql'; $warna_latar = 'warning';
				if($kolom['post_category'] == 'yii')
					$ctg = 'yii'; $warna_latar = 'success';
				
			?>
			
	
			  <div class="col-sm-12 col-md-12">


			    <div class="media">
				<a class="pull-left" href="<?php echo $ctg;?>/<?php echo $kolom['ID'];?>/<?php echo $kolom['post_seo'];?>.html">
					<div class="kotak <?php echo $warna_latar;?>" align="center"><?php echo substr($kolom['post_title'],0,1);?></div>
				</a>
				<div class="media-body">
					<a href="<?php echo $ctg;?>/<?php echo $kolom['ID'];?>/<?php echo $kolom['post_seo'];?>.html">
						<h3 class="judul sans">
						<?php echo $kolom['post_title'];?>
						</h3>
						
					</a>
				</div>

				</div>
		<hr class="featurette-divider">

                
                
                
			  </div>

		<?php } ?>
		</div>
                    
<iframe src="https://www.google.com/calendar/embed?title=Agenda&amp;showTz=0&amp;mode=AGENDA&amp;height=500&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=buqento%40gmail.com&amp;color=%231B887A&amp;ctz=America%2FNew_York" style=" border-width:0 " width="270" height="500" frameborder="0" scrolling="no"></iframe>
<!--END BACA JUGA-->


</div>	
</div>



<div class="clearfix"></div>    

<?php include("footer.php");?>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>
